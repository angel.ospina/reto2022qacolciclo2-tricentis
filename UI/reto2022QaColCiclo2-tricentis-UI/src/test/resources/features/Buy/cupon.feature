# language: es

  Característica: Ingresar cupon
    Como usuario
    Ingreso un cupon}
    para obtener un descuento

  Escenario: Ingresar un cupon
    Dado que el usuario se encuentra en la pagina web y escoge un planeta para viajar
    Cuando el usuario ingresa un cupon
    Entonces el precio final deberá ser menor al precio regular