# language: es

  Característica: Reserva de viaje
  Como administrador
  necesito controlar los datos de reserva del usuario
  para tener un control sobre las reservas

Escenario: Realizar un proceso de reserva y ingresar email incorrecto
  Dado que el usuario se encuentra en la pagina web y escoge un planeta para viajar
  Cuando el usuario llene los datos pero ingrese un email incorrecto
  Entonces el sistema me impidirá la reserva y me mostrara un mensaje explicando el error


