# language: es

Característica: Validacion del formulario de Login
    Como administrador
    quiero validar los datos ingresados
    en los campos obligatorios

    Antecedentes:
        Dado que el usuario s encuentra en el LoginPage y se dispone a ingresar los datos

    Escenario: El usuario no ingresa la contraseña
        Cuando el usuario no ingresa la contraseña en el correspondiente campo
        Entonces se mostrara un mensaje de error indicando que falta completar dicho campo

    Escenario: Login exitoso
        Cuando el usuario ingresa correctamente todos los campos del login form
        Entonces en la homepage aparecerá un mensaje de bienvenida en lugar de la opcion login