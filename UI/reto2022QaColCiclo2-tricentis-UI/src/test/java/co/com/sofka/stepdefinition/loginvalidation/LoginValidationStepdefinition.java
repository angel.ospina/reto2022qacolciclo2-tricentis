package co.com.sofka.stepdefinition.loginvalidation;

import co.com.sofka.setup.ui.Setup;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static co.com.sofka.questions.LoginNotSuccess.loginNotSuccess;
import static co.com.sofka.questions.LoginSuccess.loginSuccess;
import static co.com.sofka.tasks.fillloginform.FillLoginForm.fillLoginForm;
import static co.com.sofka.tasks.fillloginform.NoFIllLoginFormFields.noFIllLoginFormFields;
import static co.com.sofka.tasks.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.utils.ConstantVariables.NOMBRE_DESARROLLADOR;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class LoginValidationStepdefinition extends Setup {
    private static final Logger LOGGER = Logger.getLogger(LoginValidationStepdefinition.class);
    @Dado("que el usuario s encuentra en el LoginPage y se dispone a ingresar los datos")
    public void queElUsuarioSEncuentraEnElLoginPageYSeDisponeAIngresarLosDatos() {
        actorSetupTheBrowser(NOMBRE_DESARROLLADOR.getValue());
        theActorInTheSpotlight()
                .wasAbleTo(
                        openLandingPage()
                );
    }
    @Cuando("el usuario no ingresa la contraseña en el correspondiente campo")
    public void elUsuarioNoIngresaLaContrasenaEnElCorrespondienteCampo() {
        theActorInTheSpotlight().attemptsTo(
                noFIllLoginFormFields()
        );
    }
    @Entonces("se mostrara un mensaje de error indicando que falta completar dicho campo")
    public void seMostraraUnMensajeDeErrorIndicandoQueFaltaCompletarDichoCampo() {
        theActorInTheSpotlight().should(
                seeThat(
                        loginNotSuccess(), equalTo(true)
                )
        );
    }
    @Cuando("el usuario ingresa correctamente todos los campos del login form")
    public void elUsuarioIngresaCorrectamenteTodosLosCamposDelLoginForm() {
        theActorInTheSpotlight().attemptsTo(
                fillLoginForm()
        );
    }
    @Entonces("en la homepage aparecerá un mensaje de bienvenida en lugar de la opcion login")
    public void enLaHomepageApareceraUnMensajeDeBienvenidaEnLugarDeLaOpcionLogin() {
        theActorInTheSpotlight().should(
                seeThat(
                        loginSuccess(), equalTo(true)
                )
        );
    }
}
