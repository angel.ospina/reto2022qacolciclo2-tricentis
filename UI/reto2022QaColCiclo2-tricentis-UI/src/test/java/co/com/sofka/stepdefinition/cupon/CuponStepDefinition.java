package co.com.sofka.stepdefinition.cupon;

import co.com.sofka.setup.ui.Setup;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;

import static co.com.sofka.questions.ResponsePageFrom.responsePageFrom;
import static co.com.sofka.tasks.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.tasks.makebooking.BrowseToBook.browseToBook;
import static co.com.sofka.tasks.makebooking.FillCupon.fillCupon;
import static co.com.sofka.userinterfaces.booking.Booking.DISCOUNT_PRICE;
import static co.com.sofka.userinterfaces.booking.Booking.REGULAR_PRICE;
import static co.com.sofka.utils.ConstantVariables.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class CuponStepDefinition extends Setup {
    private static final Logger LOGGER = Logger.getLogger(CuponStepDefinition.class);
    @Dado("que el usuario se encuentra en la pagina web y escoge un planeta para viajar")
    public void queElUsuarioSeEncuentraEnLaPaginaWebYEscogeUnPlanetaParaViajar() {
        try{
            actorSetupTheBrowser(NOMBRE_DESARROLLADOR.getValue());
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage(),
                    browseToBook()
            );
        }catch (Exception e){
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage(),e);
        }
    }
    @Cuando("el usuario ingresa un cupon")
    public void elUsuarioIngresaUnCupon() {
        try{
            theActorInTheSpotlight().attemptsTo(
                    fillCupon()
            );
        }catch (Exception e){
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage(),e);
        }
    }
    @Entonces("el precio final deberá ser menor al precio regular")
    public void elPrecioFinalDeberaSerMenorAlPrecioRegular() {
        theActorInTheSpotlight().should(
                seeThat(MENSAJE_FAIL.getValue(),responsePageFrom(REGULAR_PRICE), Matchers.not(responsePageFrom(DISCOUNT_PRICE)))
        );
    }
}
