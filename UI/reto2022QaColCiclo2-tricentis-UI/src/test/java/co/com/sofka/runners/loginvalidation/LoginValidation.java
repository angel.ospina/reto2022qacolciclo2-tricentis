package co.com.sofka.runners.loginvalidation;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/login/loginForm.feature"},
        glue = {"co.com.sofka.stepdefinition.loginvalidation"}
)
public class LoginValidation {
}
