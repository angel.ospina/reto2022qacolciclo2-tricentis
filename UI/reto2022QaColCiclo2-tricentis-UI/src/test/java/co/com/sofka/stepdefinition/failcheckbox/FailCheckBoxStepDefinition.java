package co.com.sofka.stepdefinition.failcheckbox;

import co.com.sofka.setup.ui.Setup;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;

import static co.com.sofka.questions.ResponsePageFrom.responsePageFrom;
import static co.com.sofka.tasks.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.tasks.makebooking.BrowseToBook.browseToBook;
import static co.com.sofka.tasks.makebooking.FillBookingForm.fillBookingForm;
import static co.com.sofka.userinterfaces.booking.Booking.MENSAJE_FAIL_CONTAINER;
import static co.com.sofka.utils.ConstantVariables.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class FailCheckBoxStepDefinition extends Setup
{   private static final Logger LOGGER = Logger.getLogger(FailCheckBoxStepDefinition.class);
    @Dado("que el usuario se encuentra en la pagina web y escoge un planeta para viajar")
    public void queElUsuarioSeEncuentraEnLaPaginaWebYEscogeUnPlanetaParaViajar() {
        try{
            actorSetupTheBrowser(NOMBRE_DESARROLLADOR.getValue());
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage(),
                    browseToBook()
            );
        }catch (Exception e){
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage(),e);
        }
    }
    @Cuando("el usuario llene los datos pero ingrese un email incorrecto")
    public void elusuariollenelosdatosperoingreseunemailincorrecto() {
        try{
            theActorInTheSpotlight().attemptsTo(
                    fillBookingForm()
            );
        }catch (Exception e){
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage(),e);
        }
    }
    @Entonces("el sistema me impidirá la reserva y me mostrara un mensaje explicando el error")
    public void elSistemaMeImpidiraLaReservaYMeMostraraUnMensajeExplicandoElError() {
        theActorInTheSpotlight().should(
                seeThat(MENSAJE_FAIL.getValue(),responsePageFrom(MENSAJE_FAIL_CONTAINER),equalTo(MENSAJE_FAIL_ESPERADO.getValue()))
        );

    }
}
