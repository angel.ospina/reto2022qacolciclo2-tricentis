package co.com.sofka.runners.failchecktest;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features ="src/test/resources/features/Buy/failCheckBox.feature",
        glue = "co.com.sofka.stepdefinition.failcheckbox"
)
public class FailCheckBoxTest {
}
