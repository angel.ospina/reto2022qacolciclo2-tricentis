package co.com.sofka.userinterfaces.booking;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.xpath;


public class Booking extends PageObject {

    public static final Target PRODUCT = Target
            .the("the product")
            .located(xpath("//*[@id=\"app\"]/div/section[2]/div[4]/div/div/div[1]/div[4]/button"));
    public static final Target NAME = Target
            .the("the name")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[1]/input"));
    public static final Target EMAIL = Target
            .the("the Email")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[2]/input"));
    public static final Target SECURITYNUMBER = Target
            .the("the Social Security Number")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[3]/input"));
    public static final Target PHONENUMBER = Target
            .the("the Phone Number")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[4]/input"));
    public static final Target CUPONAPPLY = Target
            .the("the cupon apply")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[2]/div[4]/div[2]/button"));

    public static final Target MENSAJE_FAIL_CONTAINER = Target
            .the("MESSAGE FAIL")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[1]/form/div[2]/span[3]"));

    public static final Target CUPON = Target
            .the("Cupon")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[2]/div[4]/div[1]/div/input"));
    public static final Target REGULAR_PRICE = Target
            .the("regular price")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[2]/div[3]/div[2]"));
    public static final Target DISCOUNT_PRICE = Target
            .the("discount price")
            .located(xpath("//*[@id=\"app\"]/div/div[2]/section[1]/div[3]/div[2]/div[6]/div/div/strong"));


}
