package co.com.sofka.userinterfaces.login;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class LoginInterfaces extends PageObject {
    public static final Target LOGIN_BUTTON = Target
            .the("loginButton")
            .located(By.xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/li/button"));

    public static final Target USER_NAME = Target
            .the("userName")
            .located(By.xpath("//*[@id=\"login\"]/div[1]/input"));

    public static final Target PSWRD = Target
            .the("password")
            .located(By.xpath("//*[@id=\"login\"]/div[2]/input"));

    public static final Target CONFIRM_BUTTON = Target
            .the("submitButton")
            .located(By.xpath("//*[@id=\"app\"]/div/section[3]/div/div[2]/div/nav/button[2]"));

    public static final Target ERROR_VALIDATION_MESSAGE = Target
            .the("errorMessage")
            .located(By.xpath("//*[@id=\"login\"]/div[2]/span[3]"));

    public static final Target SUCCES_VALIDATION_MESSAGE = Target
            .the("welcomeMessage")
            .located(By.xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/div/button/span[1]"));
}
