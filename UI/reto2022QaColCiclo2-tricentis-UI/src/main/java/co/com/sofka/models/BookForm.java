package co.com.sofka.models;

import com.github.javafaker.Faker;

public class BookForm {

    private String name;
    private String emailAddress;
    private String socialSecurityNumber;
    private String phoneNumber;
    private static final Faker faker =new Faker();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static BookForm bookForm(){

        BookForm bookForm = new BookForm();
        bookForm.setName(faker.name().fullName());
        bookForm.setEmailAddress(faker.internet().emailAddress());
        bookForm.setSocialSecurityNumber(faker.bothify("###-##-####"));
        bookForm.setPhoneNumber(faker.number().digits(8));
        return bookForm;
    }

}
