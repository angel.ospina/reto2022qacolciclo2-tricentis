package co.com.sofka.tasks.makebooking;

import co.com.sofka.models.BookForm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;


import static co.com.sofka.interactions.Click.clickOn;
import static co.com.sofka.userinterfaces.booking.Booking.*;

public class FillBookingForm implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        BookForm bookForm = BookForm.bookForm();
        actor.attemptsTo(
                Scroll.to(NAME),
                Click.on(NAME),
                Clear.field(NAME),
                Enter.theValue(bookForm.getName()).into(NAME),

                Scroll.to(EMAIL),
                Click.on(EMAIL),
                Clear.field(EMAIL),
                Enter.theValue(bookForm.getName()).into(EMAIL),

                Scroll.to(SECURITYNUMBER),
                Click.on(SECURITYNUMBER),
                Clear.field(SECURITYNUMBER),
                Enter.theValue(bookForm.getSocialSecurityNumber()).into(SECURITYNUMBER),

                Scroll.to(PHONENUMBER),
                Click.on(PHONENUMBER),
                Clear.field(PHONENUMBER),
                Enter.theValue(bookForm.getPhoneNumber()).into(PHONENUMBER),

                clickOn(CUPON),
                Clear.field(CUPON),
                Enter.theValue(bookForm.getName()).into(CUPON),

                WaitUntil.the(MENSAJE_FAIL_CONTAINER, WebElementStateMatchers.isVisible())
                        .forNoMoreThan(10).seconds()
        );
    }
    public static FillBookingForm fillBookingForm(){
        return new FillBookingForm();
    }
}
