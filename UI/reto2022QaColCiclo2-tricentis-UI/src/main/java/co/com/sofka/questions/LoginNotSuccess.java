package co.com.sofka.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.login.LoginInterfaces.ERROR_VALIDATION_MESSAGE;
import static co.com.sofka.utils.ConstantVariables.LOGIN_NO_EXITOSO_MENSAJE;

public class LoginNotSuccess implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor){
        return (ERROR_VALIDATION_MESSAGE.resolveFor(actor).getText().contains(LOGIN_NO_EXITOSO_MENSAJE.getValue()));
    }
    public static LoginNotSuccess loginNotSuccess(){
        return new LoginNotSuccess();
    }
}
