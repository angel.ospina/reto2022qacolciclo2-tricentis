package co.com.sofka.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;

public class ResponsePageFrom implements Question<String> {
    private final Target target;
    private ResponsePageFrom(Target target) {
        this.target = target;
    }

    @Override
    public String answeredBy(Actor actor) {
        return target.resolveFor(actor).getText();
    }
    public static ResponsePageFrom responsePageFrom(Target target){
       return new ResponsePageFrom(target);
    }
}
