package co.com.sofka.tasks.makebooking;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.interactions.Click.clickOn;
import static co.com.sofka.userinterfaces.booking.Booking.PRODUCT;

public class BrowseToBook implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(PRODUCT),
                clickOn(PRODUCT)

        );

    }
    public static BrowseToBook browseToBook(){
        return new BrowseToBook();
    }
}
