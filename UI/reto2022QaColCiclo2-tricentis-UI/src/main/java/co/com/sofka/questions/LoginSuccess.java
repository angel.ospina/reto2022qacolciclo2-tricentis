package co.com.sofka.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.login.LoginInterfaces.SUCCES_VALIDATION_MESSAGE;
import static co.com.sofka.utils.ConstantVariables.LOGIN_EXITOSO_MENSAJE;

public class LoginSuccess implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor){
        return (SUCCES_VALIDATION_MESSAGE.resolveFor(actor).getText().contains(LOGIN_EXITOSO_MENSAJE.getValue()));
    }
    public static LoginSuccess loginSuccess(){
        return new LoginSuccess();
    }
}
