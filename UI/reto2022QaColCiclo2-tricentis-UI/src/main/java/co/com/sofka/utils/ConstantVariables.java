package co.com.sofka.utils;

public enum ConstantVariables
{
    NOMBRE_DESARROLLADOR("Usuario"),
    PASS("123456"),
    LOGIN_EXITOSO_MENSAJE("HELLO, JOHN"),
    LOGIN_NO_EXITOSO_MENSAJE("Password is a required field."),
    USUARIO("Ivan"),
    MENSAJE_FAIL_ESPERADO("Enter a valid e-mail address."),
    MENSAJE_FAIL("La respuesta del sistema"),
    CUPON_DESCUENTO("CUPONASO");
    private final String value;
    public String getValue() {
        return value;
    }
    ConstantVariables(String value) {
        this.value = value;
    }
}
