package co.com.sofka.tasks.fillloginform;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.userinterfaces.login.LoginInterfaces.*;
import static co.com.sofka.utils.ConstantVariables.USUARIO;

public class NoFIllLoginFormFields implements Task {
    @Override
    public <T extends Actor> void performAs(T actor){
        actor.attemptsTo(
                Click.on(LOGIN_BUTTON),
                Enter.theValue(USUARIO.getValue()).into(USER_NAME),

                Click.on(CONFIRM_BUTTON),
                WaitUntil.the(ERROR_VALIDATION_MESSAGE, WebElementStateMatchers.isVisible())
                        .forNoMoreThan(10)
                        .seconds()
                );
    }
    public static NoFIllLoginFormFields noFIllLoginFormFields(){
        return new NoFIllLoginFormFields();
    }

}
