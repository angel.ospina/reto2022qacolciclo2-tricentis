package co.com.sofka.tasks.makebooking;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.interactions.Click.clickOn;
import static co.com.sofka.userinterfaces.booking.Booking.*;
import static co.com.sofka.utils.ConstantVariables.CUPON_DESCUENTO;

public class FillCupon implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(CUPON),
                clickOn(CUPON),
                Clear.field(CUPON),
                Enter.theValue(CUPON_DESCUENTO.getValue()).into(CUPON),

                WaitUntil.the(CUPONAPPLY, WebElementStateMatchers.isClickable())
                        .forNoMoreThan(10).seconds(),
                Scroll.to(CUPONAPPLY),
                clickOn(CUPONAPPLY)
        );
    }
    public static FillCupon fillCupon(){
        return new FillCupon();
    }
}
