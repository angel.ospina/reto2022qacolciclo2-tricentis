package co.com.sofka.models;

public class ConvertirNumeros {
    private Integer numero;

    public Integer getNumero() {
        return numero;
    }

    public Integer withNumero(Integer numero) {
        this.numero = numero;
        return numero;
    }

}
