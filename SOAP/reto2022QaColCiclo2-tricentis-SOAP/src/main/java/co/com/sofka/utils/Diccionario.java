package co.com.sofka.utils;

public class Diccionario {

    public static final Integer NUMERO_A_CONVERTIR = 35;
    public static final String NUMERO_EN_TEXTO = "thirty five dollars";
    public static final String ERROR = "number too large";

    public static final String NOMBRE_LENGUAJE ="korean";

    public static final String CODIGO_LENGUAJE = "kor";

    public static final String ERROR_LENGUAJE = "Language name not found!";


}
