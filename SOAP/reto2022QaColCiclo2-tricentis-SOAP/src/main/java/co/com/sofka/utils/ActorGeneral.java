package co.com.sofka.utils;
public enum ActorGeneral {

    ACTOR ("Mónica");
    private final String value;

    public String getValue(){
        return value;
    }
    ActorGeneral(String value) {
        this.value = value;
    }
}
