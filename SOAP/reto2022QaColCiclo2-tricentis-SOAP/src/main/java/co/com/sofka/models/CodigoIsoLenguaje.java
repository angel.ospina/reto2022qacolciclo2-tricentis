package co.com.sofka.models;

public class CodigoIsoLenguaje {

    private String codigo;

    public String getCodigo() {
        return codigo;
    }

    public String withCodigo(String codigo) {
        this.codigo = codigo;
        return codigo;
    }


}
