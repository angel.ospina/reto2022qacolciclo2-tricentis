#language:es

    Característica: código ISO del lenguaje de un país
        como usuario
        necesito conocer el código ISO del lenguaje de un país
        para obtener la información correcta


    Escenario: Obtener código ISO del lenguaje de un país
        Dado que un usuario se encuentra en el portal web
        Cuando el usuario ingresa el nombre del lenguaje y genera la consulta
        Entonces el usuario visualizará en pantalla el código ISO del lenguaje consultado
