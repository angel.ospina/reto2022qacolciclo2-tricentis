#language: es

    Característica: Número a dólares
        como usuario
        necesito convertir cierta cantidad de números en palabras representadas en dólares
        para visualizar si la escritura es correcta

    Escenario: Convertir cierta cantidad de números a escritura en dólares
        Dado que un usuario se encuentra en el portal web
        Cuando el usuario ingresa el número a convertir y genera la consulta
        Entonces el usuario visualizará la cantidad especificada en palabras representadas en dólares