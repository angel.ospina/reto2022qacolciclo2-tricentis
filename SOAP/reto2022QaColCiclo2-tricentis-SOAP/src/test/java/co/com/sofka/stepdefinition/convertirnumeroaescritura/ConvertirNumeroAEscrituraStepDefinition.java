package co.com.sofka.stepdefinition.convertirnumeroaescritura;

import co.com.sofka.models.ConvertirNumeros;
import co.com.sofka.setup.SetUp;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.log4j.Logger;

import java.nio.charset.StandardCharsets;

import static co.com.sofka.questions.ReturnStringValue.returnStringValue;
import static co.com.sofka.tasks.DoPost.doPost;
import static co.com.sofka.utils.Diccionario.*;
import static co.com.sofka.utils.FileUtilities.readFile;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;


public class ConvertirNumeroAEscrituraStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(ConvertirNumeroAEscrituraStepDefinition.class);

    private static final String URL_BASE = "https://www.dataaccess.com";

    private static final String RESOURCE = "/webservicesserver/NumberConversion.wso";

    private static final String CONVERTIR_NUMEROS_XML = System.getProperty("user.dir") + "\\src\\test\\resources\\files\\convertirNumeroAEscritura.xml";

    private static final String CONVERTIR_NUMEROS = "[numero]";
    private String bodyRequest;

    @Dado("que un usuario se encuentra en el portal web")
    public void queUnUsuarioSeEncuentraEnElPortalWeb() {
        try {
            setUpLog4j2();
            actor.can(CallAnApi.at(URL_BASE));

            headers.put("Content-Type", "text/xml;charset=UTF-8");
            headers.put("SOAPAction", "");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("el usuario ingresa el número a convertir y genera la consulta")
    public void elUsuarioIngresaElNumeroAConvertirYGeneraLaConsulta() {
        try {
            bodyRequest = defineBodyRequest(numero());
            LOGGER.info(bodyRequest);

            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE)
                            .withHeaders(headers)
                            .andBodyRequest(bodyRequest)
            );

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }


    }
    @Entonces("el usuario visualizará la cantidad especificada en palabras representadas en dólares")
    public void elUsuarioVisualizaraLaCantidadEspecificadaEnPalabrasRepresentadasEnDolares() {
        String soapResponse = new String(LastResponse.received().answeredBy(actor).asByteArray(), StandardCharsets.UTF_8);
        LOGGER.info(soapResponse);

        actor.should(
                seeThatResponse(
                        "El código de respuesta debe ser: " + HttpStatus.SC_OK,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                ),
                seeThat(
                        "El error debe ser: " + ERROR,
                        returnStringValue(soapResponse),
                        containsString(NUMERO_EN_TEXTO)
                )
        );

    }

    private String defineBodyRequest(Integer numero){
        return readFile(CONVERTIR_NUMEROS_XML).replace(CONVERTIR_NUMEROS, numero.toString());
    }

    private Integer numero() {
        ConvertirNumeros convertirNumeros = new ConvertirNumeros();
        return convertirNumeros.withNumero(NUMERO_A_CONVERTIR);
    }

}
