package co.com.sofka.stepdefinition.obtenercodigoiso;


import co.com.sofka.models.CodigoIsoLenguaje;
import co.com.sofka.setup.SetUp;
import co.com.sofka.stepdefinition.convertirnumeroaescritura.ConvertirNumeroAEscrituraStepDefinition;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.log4j.Logger;

import java.nio.charset.StandardCharsets;

import static co.com.sofka.questions.ReturnStringValue.returnStringValue;
import static co.com.sofka.tasks.DoPost.doPost;
import static co.com.sofka.utils.Diccionario.*;
import static co.com.sofka.utils.FileUtilities.readFile;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class ObtenerCodigoISOLenguajeStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(ConvertirNumeroAEscrituraStepDefinition.class);

    private static final String URL_BASE = "http://webservices.oorsprong.org";

    private static final String RESOURCE = "/websamples.countryinfo/CountryInfoService.wso";

    private static final String OBTENER_CODIGO_ISO_XML = System.getProperty("user.dir") + "\\src\\test\\resources\\files\\obtenerCodigoISOLenguaje.xml";

    private static final String OBTENER_CODIGO_ISO = "[nombreLenguaje]";
    private String bodyRequest;

    @Dado("que un usuario se encuentra en el portal web")
    public void queUnUsuarioSeEncuentraEnElPortalWeb() {
        try {
            setUpLog4j2();
            actor.can(CallAnApi.at(URL_BASE));

            headers.put("Content-Type", "text/xml;charset=UTF-8");
            headers.put("SOAPAction", "");

        } catch (Exception e) {
        LOGGER.error(e.getMessage(), e);
        }

    }
    @Cuando("el usuario ingresa el nombre del lenguaje y genera la consulta")
    public void elUsuarioIngresaElNombreDelLenguajeYGeneraLaConsulta() {
        try {
            bodyRequest = defineBodyRequest(codigoLenguaje());
            LOGGER.info(bodyRequest);

            actor.attemptsTo(
                    doPost()
                            .usingTheResource(RESOURCE)
                            .withHeaders(headers)
                            .andBodyRequest(bodyRequest)
            );

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }
    @Entonces("el usuario visualizará en pantalla el código ISO del lenguaje consultado")
    public void elUsuarioVisualizaraEnPantallaElCodigoISODelLenguajeConsultado() {
        String soapResponse = new String(LastResponse.received().answeredBy(actor).asByteArray(), StandardCharsets.UTF_8);
        LOGGER.info(soapResponse);

        actor.should(
                seeThatResponse(
                        "El código de respuesta debe ser: " + HttpStatus.SC_OK,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                ),
                seeThat(
                        "El error debe ser: " + ERROR_LENGUAJE,
                        returnStringValue(soapResponse),
                        containsString(CODIGO_LENGUAJE)
                )
        );

    }

    private String defineBodyRequest(String obtenerCodigo){
        return readFile(OBTENER_CODIGO_ISO_XML).replace(OBTENER_CODIGO_ISO, obtenerCodigo.toString());
    }

    private String codigoLenguaje() {
        CodigoIsoLenguaje codigoIsoLenguaje = new CodigoIsoLenguaje();
        return codigoIsoLenguaje.withCodigo(NOMBRE_LENGUAJE);
    }


}
