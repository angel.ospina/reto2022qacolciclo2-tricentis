package co.com.sofka.runner.obtenercodigoiso;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/obtenercodigoiso/obtenerCodigoISOLenguaje.feature"},
        glue = {"co.com.sofka.stepdefinition.obtenercodigoiso"},
        tags = ""
)

public class ObtenerCodigoISOLenguajeTest {
}
