package co.com.sofka.runner.convertirnumeroaescritura;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/convertirnumeroaescritura/convertirNumerosAEscritura.feature"},
        glue = {"co.com.sofka.stepdefinition.convertirnumeroaescritura"},
        tags = ""
)

public class ConvertirNumeroAEscrituraTest {
}
