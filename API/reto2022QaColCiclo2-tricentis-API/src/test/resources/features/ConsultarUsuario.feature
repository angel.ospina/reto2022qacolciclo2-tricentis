# language: es

  Característica: Consultar Usuario
    yo como usuario deseo poder consultar la
    informacion de un usuario en la pagina web de adopcion de mascotas

  Escenario: Get Consultar Usuario
    Dado  que el usuario tiene toda la informacion para realizar la consulta
    Cuando realiza una peticion get al servidor
    Entonces recibe una respuesta positiva para consultar un usuario






