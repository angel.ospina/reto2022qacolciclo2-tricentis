package co.com.sofka.stepdefinition;

import co.com.sofka.models.CreateUserResponseModel;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;

import java.util.HashMap;

import static co.com.sofka.tasks.DoPost.doPost;
import static co.com.sofka.utils.FileUtilities.readFile;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.notNullValue;

public class CrearUsuarioStepDefinition {

    private final HashMap<String,String> header = new HashMap<>();
    private String bodyRequest;

    private final Actor alexandra = Actor.named("alexandra");

    @Dado("que el usuario tiene toda la informacin de registro")
    public void queElUsuarioTieneTodaLaInformacinDeRegistro() {
        String URL_BASE = "https://petstore.swagger.io/v2";
        alexandra.can(CallAnApi.at(URL_BASE));
        header.put("Content-type","application/json");
        bodyRequest = defineBodyRequest();
    }
    @Cuando("realiza una peticion post al servidor")
    public void realizaUnaPeticionPostAlServidor() {
        String RESOURCE = "/user";
        alexandra.attemptsTo(
                doPost()
                        .usingResource(RESOURCE)
                        .withHeader(header)
                        .andBody(bodyRequest)
        );
    }
    @Entonces("recibe una respuesta positiva de informacion con el usuario creado")
    public void recibeUnaRespuestaPositivaDeInformacionConElUsuarioCreado() {
        CreateUserResponseModel respuesta = SerenityRest.lastResponse().as(CreateUserResponseModel.class);
        alexandra.should(
                seeThatResponse(
                        "El codigo de respuesta debe ser: " + respuesta.getCode(),
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                ),
                seeThat(
                "el mensaje enviado debe ser un string no null",
                response -> respuesta.getMessage(),
                notNullValue()
        )
        );
    }

    private String defineBodyRequest(){
        String db_data = "src/test/resources/db/user.json";
        return readFile(db_data);
    }
}
