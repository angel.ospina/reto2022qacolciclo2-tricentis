package co.com.sofka.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;

import java.nio.charset.StandardCharsets;


import static co.com.sofka.tasks.DoGet.doGet;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;

public class ConsultarUsuarioStepDefinition {

    private final Actor alexandra = Actor.named("alexandra");

    @Dado("que el usuario tiene toda la informacion para realizar la consulta")
    public void queElUsuarioTieneTodaLaInformacionParaRealizarLaConsulta() {
        String URL_BASE = "https://petstore.swagger.io/v2";
        alexandra.can(CallAnApi.at(URL_BASE));
    }
    @Cuando("realiza una peticion get al servidor")
    public void realizaUnaPeticionGetAlServidor() {
        String RESOURCE = "/user/string";
        alexandra.attemptsTo(
            doGet()
                    .usingTheResource(RESOURCE)
        );
    }
    @Entonces("recibe una respuesta positiva para consultar un usuario")
    public void recibeUnaRespuestaPositivaParaConsultarUnUsuario() {
        String response=new String(LastResponse.received().answeredBy(alexandra).asByteArray(), StandardCharsets.UTF_8);
        alexandra.should(
                seeThatResponse(
                        "el codigo de respuesta debe ser: "+ HttpStatus.SC_OK,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                ),
                seeThat(
                        "el mensaje enviado debe ser Created",
                        resp -> response,
                        containsString("string")


                )
        );
    }

}
