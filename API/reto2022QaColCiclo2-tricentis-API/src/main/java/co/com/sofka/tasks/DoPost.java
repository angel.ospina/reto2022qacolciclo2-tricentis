package co.com.sofka.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;

import java.util.HashMap;
import java.util.Map;

public class DoPost implements Task {
    private String resource;
    private HashMap<String,String> header;
    private String body;

    public DoPost usingResource(String resource){
        this.resource = resource;
        return this;
    }


    public DoPost withHeader(Map<String,String> header){
        this.header = (HashMap<String, String>) header;
        return this;
    }

    public DoPost andBody(String body){
        this.body = body;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(resource).with(
                        requestSpecification -> requestSpecification.relaxedHTTPSValidation()
                                .headers(header)
                                .body(body)
                )
        );
    }

    public static DoPost doPost(){
        return new DoPost();
    }
}
